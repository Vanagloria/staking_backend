exports.up = function(knex) {
    return knex.schema.createTable('tx', function(t) {
        t.uuid('id').primary().defaultTo(knex.raw('uuid_generate_v4()'));

        t.string('coin').nullable();
        t.text('amount').nullable();
        t.string('address').nullable();
        t.text('txid').notNull();
        t.enum('type', ['stake', 'withdraw']).notNull();
        t.enum('status', ['success', 'fail', 'processing', 'new']).notNull();

        t.dateTime('created_at').notNull();
        t.dateTime('updated_at').nullable();
    });
};

exports.down = function(knex) {
    return knex.raw('DROP TABLE tx CASCADE');
};
