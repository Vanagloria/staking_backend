const swaggerAutogen = require('swagger-autogen')();

const doc = {
    info: {
        version: '1.0.0',      // by default: '1.0.0'
        title: 'Core REST API',        // by default: 'REST API'
        description: '',  // by default: ''
    },
    host: 'localhost:777',      // by default: 'localhost:3000'
    basePath: '/',  // by default: '/'
    schemes: ['http'],   // by default: ['http']
    consumes: ['application/json'],  // by default: ['application/json']
    produces: ['application/json'],  // by default: ['application/json']
    tags: [        // by default: empty Array
        {
            name: 'stage',         // Tag name
            description: 'dev',  // Tag description
        },
        // { ... }
    ],
    securityDefinitions: {},  // by default: empty object
    definitions: {},          // by default: empty object
};

const outputFile = './docs/swagger-output.json';
const endpointsFiles = ['./app.js'];

/* NOTE: if you use the express Router, you must pass in the
   'endpointsFiles' only the root file where the route starts,
   such as: index.js, app.js, routes.js, ... */

swaggerAutogen(outputFile, endpointsFiles, doc);