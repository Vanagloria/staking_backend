const responseStatus = {
    SUCCESS: "success",
    FAIL: "fail"
};

const transactionStatus = {
    NEW: "new",
    PROCESSING: "processing",
    FAIL: "fail",
    SUCCESS: "success"
};

const transactionType = {
    STAKE: "stake",
    WITHDRAW: "withdraw"
};

module.exports = {responseStatus, transactionStatus, transactionType};