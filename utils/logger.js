const bunyan = require('bunyan');

module.exports.createLogger = function createLogger(logName) {
    return bunyan.createLogger({
        name: logName,
        src: process.env.LOG_LEVEL === "debug",
        streams: [
            {
                level: process.env.LOG_LEVEL,
                stream: process.stdout,
                formatter: 'pretty'
            }
        ]
    });
}