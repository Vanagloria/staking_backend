const ErrorWithCode = require('./ErrorWithCode')

class ValidationError extends ErrorWithCode {
    constructor(message) {
        super(message, 0x01);
    }
}

module.exports.ValidationError = ValidationError;