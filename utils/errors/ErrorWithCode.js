class ErrorWithCode extends Error {
    constructor(message, code) {
        super(message);
        this._code = code;
        this._message = message;
    }
    get code() {
        return this._code;
    }

    get message() {
        return this._message;
    }

    toJSON() {
        return {
            code: this._code,
            message: this._message
        }
    }
}

module.exports = ErrorWithCode