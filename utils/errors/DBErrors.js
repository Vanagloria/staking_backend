const ErrorWithCode = require('./ErrorWithCode')

class ObjectNotFoundError extends ErrorWithCode {
    constructor(message) {
        super(message, 0xA1);
    }
}

module.exports.ObjectNotFoundError = ObjectNotFoundError;