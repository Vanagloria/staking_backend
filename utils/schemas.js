const Joi = require('joi');
const {transactionType} = require('./enums')
const {ValidationError} = require('./errors/GeneralErrors')

module.exports.validateObject = function validateBody(schema, body) {
    const validation = schema.validate(body);
    if (validation.error) {
        return new ValidationError(validation.error.details);
    }
    return null;
}

module.exports.txSchema = Joi.object().keys({
    txid: Joi.string().regex(/^0x([A-Fa-f0-9]{64})$/).required(),
    coin: Joi.string().regex(/[a-zA-Z]{3,6}/).required(),
    amount: Joi.string().required(),
    address: Joi.string().regex(/^0x[a-fA-F0-9]{40}$/).required(),
    type: Joi.string().valid(transactionType.STAKE, transactionType.WITHDRAW).required()
});

module.exports.idSchema = Joi.string().regex(/\b[0-9a-f]{8}\b-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-\b[0-9a-f]{12}\b/).required();