const { BigNumber } = require("bignumber.js");

class CurrentRate {
    _currentRate = null;
    _lastUpdated = null;
    setCurrentRate(newRate) {
        this._currentRate = new BigNumber(newRate);
        this._lastUpdated = Date.now();
    }
    getCurrentRate(precision = 2) {
        return {
            rate: this._currentRate.toPrecision(precision).toString(),
            updated: new Date(this._lastUpdated).toISOString()
        }
    }
}

const currentRate = new CurrentRate();
module.exports = currentRate;