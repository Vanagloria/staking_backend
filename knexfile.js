require('dotenv').config({
    path: __dirname + "/.env"
});
console.log(__dirname);
module.exports = {
    development: {
        client: 'pg',
        connection: `postgres://${process.env.PG_USER}:${process.env.PG_PASSWORD}@${process.env.PG_HOST}:${process.env.PG_PORT}/${process.env.PG_DB}`,
        migrations: {
            directory: __dirname + '/db/migrations'
        }
    }
};