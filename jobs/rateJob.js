const TimingJob = require('./timingJob');
const axios = require("axios");
const currentRate = require('../utils/currentRate')

class RateJob extends TimingJob {
    _rate_url = process.env.RATE_ENDPOINT;
    /**
     * {
     *     "0xcdb9d30a3ba48cdfcb0ecbe19317e6cf783672f1": {
     *       "usd": 0.950372
     *     }
     * }
     */
    async doJob() {
        const response = await axios.get(this._rate_url);
        const innerValue = Object.values(response.data)[0];
        if (response.status === 200 && innerValue && innerValue.usd)
            currentRate.setCurrentRate(innerValue.usd);
    }
}

module.exports = RateJob;