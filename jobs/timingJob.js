class TimingJob {
    _stop = false;
    _timeout = 1000;
    sleep(ms) {
        return new Promise((resolve) => {
            setTimeout(resolve, ms);
        });
    }

    setTimeout(timeout) {
        this._timeout = timeout;
    }

    pause() {
        this._stop = true;
    }

    proceed() {
        this._stop = false;
        this.exec();
    }

    async dojob() {

    };

    async exec() {
        while (!this._stop) {
            await this.doJob();
            await this.sleep(this._timeout);
        }
    }
}

module.exports = TimingJob;