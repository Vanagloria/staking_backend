const express = require('express');

const router = express.Router();
const currentRate = require('../utils/currentRate')

router.get('/health', (req, res) => {
    return res.status(200).json({
        status: 'success'
    });
});

router.get('/rate', (req, res) => {
    return res.status(200).json({
        status: 'success',
        result: {
            'usd' : currentRate.getCurrentRate(5)
        }
    })
})
module.exports = router;