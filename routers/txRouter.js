const express = require('express');
const router = express.Router();

const Transaction = require('../model/transaction')
const enums = require('../utils/enums')
const logger = require('../utils/logger')
const log = logger.createLogger("tx-router")
const {ObjectNotFoundError} = require('../utils/errors/DBErrors')
const {validateObject, txSchema, idSchema} = require('../utils/schemas')

router.get('/fetch/', async(req, res) => {
    const address = req.query.address
    try {
        const transactions = await (address ? Transaction.query().where("address", "=", address) : Transaction.query());
        return res.json({
            status: enums.responseStatus.SUCCESS,
            result: transactions
        });
    } catch (e) {
        log.error(e);
        return res.status(500).json({
            status: enums.responseStatus.FAIL,
            error: e
        });
    }
});

router.get('/:id', async (req, res) => {
    const internalTxId = req.params.id;
    const idValidationError = validateObject(idSchema, internalTxId);
    if (idValidationError) {
        return res.status(400).json({
            status: enums.responseStatus.FAIL,
            error: idValidationError
        });
    }
    const foundTx = await Transaction.query().findById(internalTxId);
    if (!foundTx) {
        const error = new ObjectNotFoundError(`Transaction with id ${internalTxId} not found.`);
        log.error(error);
        return res.status(404).json({
            status: enums.responseStatus.FAIL,
            error
        });
    } else {
        return res.status(200).json({
            status: enums.responseStatus.SUCCESS,
            result: foundTx
        })
    }
});

router.post('/', async (req, res) => {
    const bodyValidationError = await validateObject(txSchema, req.body);
    if (bodyValidationError) {
        return res.status(400).json({
            status: enums.responseStatus.FAIL,
            error: bodyValidationError
        });
    }
    try {
        const tx = await Transaction.query().insert({
            ...req.body,
            status: enums.transactionStatus.NEW
        });
        return res.json({
            status: enums.responseStatus.SUCCESS,
            result: tx
        });
    } catch (e) {
        log.error(e);
        return res.status(500).json({
            status: enums.responseStatus.FAIL,
            error: e
        });
    }
});

router.put('/:id', async (req, res) => {
    const internalTxId = req.params.id;
    const idValidationError = validateObject(idSchema, internalTxId);
    if (idValidationError) {
        return res.status(400).json({
            status: enums.responseStatus.FAIL,
            error: idValidationError
        });
    }
    const foundTx = await Transaction.query().findById(internalTxId);
    if (!foundTx) {
        const error = new ObjectNotFoundError(`Transaction with id ${internalTxId} not found.`);
        log.error(error);
        return res.status(404).json({
            status: enums.responseStatus.FAIL,
            error
        });
    } else {
        try {
            const tx = await Transaction.query().updateAndFetchById(req.params.id, {
                ...req.body
            });
            return res.json({
                status: enums.responseStatus.SUCCESS,
                result: tx
            });
        } catch (e) {
            log.error(e);
            return res.status(500).json({
                status: enums.responseStatus.FAIL,
                error: e
            });
        }
    }
});

router.delete('/:id', async (req, res) => {
    const internalTxId = req.params.id;
    const idValidationError = validateObject(idSchema, internalTxId);
    if (idValidationError) {
        return res.status(400).json({
            status: enums.responseStatus.FAIL,
            error: idValidationError
        });
    }
    const foundTx = await Transaction.query().findById(internalTxId);
    if (!foundTx) {
        const error = new ObjectNotFoundError(`Transaction with id ${internalTxId} not found.`);
        log.error(error);
        return res.status(404).json({
            status: enums.responseStatus.FAIL,
            error
        });
    } else {
        try {
            await Transaction.query().deleteById(req.params.id);
            return res.json({
                status: enums.responseStatus.SUCCESS,
                result: {}
            });
        } catch (e) {
            log.error(e);
            return res.status(500).json({
                status: enums.responseStatus.FAIL,
                error: e
            });
        }
    }
});

module.exports = router;