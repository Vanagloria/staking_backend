require('dotenv').config();
const express = require('express');
const bodyParser = require('body-parser');

const {createLogger} = require("./utils/logger");
const log = createLogger("general")
const app = express();

const port = process.env.PORT;

const generalRouter = require('./routers/generalRouter');
const txRouter = require('./routers/txRouter');
const RateJob = require('./jobs/rateJob')

app.use(bodyParser.json({ type: 'application/json' }));

app.use((req, res, next) => {
  log.info({
    method: req.method,
    path: req.path,
    body: req.body,
    params: req.params,
    query: req.query
  }, "Incoming request");
  next();
});

app.use('/', generalRouter);
app.use('/transaction', txRouter);

app.use((req, res, next) => {
  return res.status(404);
});

const rateJob = new RateJob();
rateJob.setTimeout(1000);

app.listen(port, () => {
  log.info(`App listening at http://localhost:${port}`);
  rateJob.exec();
});