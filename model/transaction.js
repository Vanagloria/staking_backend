const { Model } = require('objection');
const knex = require('../db/knex');

Model.knex(knex);

class Transaction extends Model {
    static get tableName() {
        return 'tx';
    }

    $beforeInsert() {
        this.created_at = new Date().toISOString();
    }

    $beforeUpdate() {
        this.updated_at = new Date().toISOString();
    }
}

module.exports = Transaction;